# historias-de-fantasmas-de-un-anticuario

Traducción propia del libro «Ghost stories of an antiquary» de Montague Rhodes James, que se encuentra en dominio público.

(0) 2016, Ariel Becker. Todos los derechos reversados y cocinados.

## Traducción

Las traducciones de cada cuento se ofrecen en un archivo por separado para facilitar el trabajo. Una vez que el libro se traduzca por completo, se unificarán en un solo volumen.

## Colaboraciones

Por el momento el proyecto está cerrado a las colaboraciones, pero es posible reportar cualquier error mediante el uso de [esta herramienta](https://bitbucket.org/arielsbecker/historias-de-fantasmas-de-un-anticuario/issues?status=new&status=open).

## Donaciones

Para ayudar económicamente, he abierto los siguientes canales de recepción de donaciones.

### Criptodivisas

#### Mediante Bitcoin

Sírvase enviar BTC a [1MdfB7Pn8revosFZsoighKGd3VZ5sgjtek](https://www.blockchain.com/btc/address/1MdfB7Pn8revosFZsoighKGd3VZ5sgjtek).

#### Mediante Ether

Sírvase enviar ETH a [0xC12Df5F402A8B8BEaea49ed9baD9c95fCcbfE907](https://etherscan.io/address/0xC12Df5F402A8B8BEaea49ed9baD9c95fCcbfE907).

#### Mediante Nebulas

Sírvase enviar NAS a [n1cZF8Kp4o8PmSyhPvmkn4VEp3phUXP1Qke](https://explorer.nebulas.io/#/address/n1cZF8Kp4o8PmSyhPvmkn4VEp3phUXP1Qke).

## Disponibilidad online

Además de este repositorio, se puede encontrar una versión mobile-friendly en [Cent](https://beta.cent.co/+psy8an).

La versión disponible en [Amazon](https://www.amazon.com/dp/B01MATHYPR) queda obsoleta y ya no es válida.